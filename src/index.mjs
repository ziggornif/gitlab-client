function processRequest({url, method, headers, params, data}) {
  const query = {headers}
  const reqUrl = new URL(url)
  switch (method) {
    case 'GET':
      query.method = method
      if (params) {
        for(const key of Object.keys(params)) {
          reqUrl.searchParams.set(key, params[key])
        }
      }
      break;
    case "POST":
    case "PUT":
    case "PATCH": {
      query.method = method
      query.body = JSON.stringify(data);
      break;
    }
  
    default:
      break;
  }
  return fetch(reqUrl.toString(), query).then((d) => d.json());
}

function createClient (url, token) {
  return new Proxy({}, {
    get(_, key) {
      const method = key.toUpperCase();
      if (["GET", "POST", "PUT", "DELETE", "PATCH"].includes(method)) {
        return async function({data, params} = {}) {
          return processRequest({url, method, headers: {
            'PRIVATE-TOKEN': token
          }, data, params})
        }
      }
      return createClient(`${url}/${key}`, token);
    }
  })
}

export default createClient