# Gitlab client

Gitlab API Client based on Javascript proxy.

## Requirements

NodeJS v17.x to use experimental fetch.

## Usage

```js
import createClient from "./index.mjs";

const client = createClient('https://gitlab.com/api/v4', 'YOUR_GITLAB_TOKEN_HERE')

// Get user projects
// https://gitlab.com/api/v4/users/00000/projects
const userProjs = await client.users[00000].projects.get();
console.log(userProjs);

// Get project events
// https://gitlab.com/api/v4/projects/00000/events
const projEvents = await client.projects[25481792].events.get();
console.log(projEvents)
```

> Inspired by [@DavidWells](https://twitter.com/DavidWells) recent work https://gist.github.com/DavidWells/93535d7d6bec3a7219778ebcfa437df3